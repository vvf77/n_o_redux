import React from 'react';
import {define, Component} from 'react-mvx';
import TodoModel from "../models/TodoModel";

@define
class AddTodo extends Component {
    static props = {
        todos: TodoModel.Collection
    };
    static state = {
        value: String.value('').has.check(v => v && String(v).length >= 2)
    };

    render() {
        const valueLink = this.state.linkAt('value');
        return (<form onSubmit={this.onAdd}>
            <input type="text"
                   {...valueLink.props}
                   className={this.state.isValid('value') ? '' : 'invalid'}
            />
            <input type="submit" value="Add todo"/>
            {!this.state.isValid('value') && <div>{this.state.getValidationError('value')}</div>}
        </form>);
    }

    onAdd = (event) => {
        event.preventDefault();
        this.props.todos.createTodo(this.state.value);
        // eslint-disable-next-line
        this.state.value = '';
    };
}

export default AddTodo;