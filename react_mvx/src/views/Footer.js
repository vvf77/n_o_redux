import React from 'react';

function Footer({setCompleteFilter, filterBy}) {
    return (<div>
        <span>Show: </span>
        <button onClick={() => setCompleteFilter(null)} disabled={filterBy===null}>
            All
        </button>
        <button onClick={() => setCompleteFilter(false)}  disabled={filterBy===false}>
            Active
        </button>
        <button onClick={() => setCompleteFilter(true)}  disabled={filterBy===true}>
            Completed
        </button>
    </div>);
}

export default Footer
