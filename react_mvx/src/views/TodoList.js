import React from 'react';
import Todo from "./Todo";

function TodoList({todos, filterBy}) {
    if (filterBy !== null) {
        todos = todos.filter(todo => todo.completed === filterBy)
    }
    return (<ul>
        {todos.map((todo, npp) => <Todo
            key={npp}
            todo={todo}
            onClick={() => {todo.completed = !todo.completed; todo.save()}}
        />)}
    </ul>)
}


export default TodoList;