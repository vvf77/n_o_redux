import React from 'react';
import {define, Component} from 'react-mvx';
import '../App.css';
import AddTodo from "./AddTodo";
import TodoList from "./TodoList";
import Footer from "./Footer";
import TodoModel from "../models/TodoModel";

@define
class App extends Component {
    static state = {
        todos: TodoModel.Collection,
        filterBy: Boolean.value(null)
    };
    componentWillMount(){
        this.state.todos.fetch();
    }
    render() {
        if(this.state.todos.hasPendingIO()){
            return <div className="App">
                <div className="loading">Loading...</div>
            </div>
        }
        return (
            <div className="App">
                <AddTodo todos={this.state.todos}/>
                <TodoList
                    todos={this.state.todos}
                    filterBy={this.state.filterBy}
                />
                <Footer
                    setCompleteFilter={this.setCompleteFilter}
                    filterBy={this.state.filterBy}
                />
            </div>
        );
    }

    setCompleteFilter = (newFilterBy) => {
        // eslint-disable-next-line
        this.state.filterBy = newFilterBy;
    };

}
export default App;
