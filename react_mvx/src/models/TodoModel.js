import {Record, define} from 'type-r';
import {restfulIO} from "type-r/endpoints/restful";

const TODOS_ENDPOINT = 'https://jsonplaceholder.typicode.com/todos';

@define
class TodoModel extends Record {
    static endpoint = restfulIO(TODOS_ENDPOINT);
    static attributes = {
        title: String.value(''),
        completed: Boolean.value(false),
        userId: Number
    };
    static collection = {
        createTodo(title) {
            const addedTodos = this.add({title});
            addedTodos.forEach(todo => todo.save());
        }
    }
}

export default TodoModel;