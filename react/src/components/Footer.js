import React from 'react';

function Footer({setCompleterFilter, filterBy}) {
    return (<div>
        <span>Show: </span>
        <button onClick={() => setCompleterFilter(null)} disabled={filterBy===null}>
            All
        </button>
        <button onClick={() => setCompleterFilter(false)} disabled={filterBy===false}>
            Active
        </button>
        <button onClick={() => setCompleterFilter(true)} disabled={filterBy===true}>
            Completed
        </button>
    </div>);
}

export default Footer
