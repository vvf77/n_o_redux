import React from 'react';
import Todo from "./Todo";

function TodoList({todos, filterBy, toggleCompleted}) {
    if (filterBy !== null) {
        todos = todos.filter(todo => todo.completed === filterBy)
    }
    return (<ul>
        {todos.map((todo, npp) => <Todo
            key={npp}
            todo={todo}
            onClick={() => toggleCompleted(todo)}
        />)}
    </ul>)
}

export default TodoList;