import React, {Component} from 'react';

class AddTodo extends Component {
    state = {
        value: ''
    };

    render() {
        return (<form onSubmit={this.onAdd}>
            <input type="text"
                   onChange={this.onChange}
                   value={this.state.value}
            />
            <input type="submit" value="Add todo"/>
        </form>);
    }

    onChange = (event) => {
        this.setState({value: event.target.value});
    };

    onAdd = (event) => {
        event.preventDefault();
        this.props.addTodo(this.state.value);
        this.setState({value: ''})
    };
}

export default AddTodo