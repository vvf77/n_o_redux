import React, {Component} from 'react';
import '../App.css';
import AddTodo from "./AddTodo";
import TodoList from "./TodoList";
import Footer from "./Footer";

const TODOS_ENDPOINT = 'https://jsonplaceholder.typicode.com/todos';

class App extends Component {
    state = {
        todos: [],
        filterBy: null,
        isLoading: false
    };

    componentWillMount() {
        this.setState({isLoading: true});
        fetch(TODOS_ENDPOINT)
            .then(responce => responce.json())
            .then(todos => {
                this.setState({
                    isLoading: false,
                    todos
                });
            }).catch(error => {
            console.trace(error);
            this.setState({
                isLoading: false
            });
        });
    }

    render() {
        if (this.state.todos.isLoading) {
            return <div className="App">
                <div className="loading">Loading...</div>
            </div>
        }
        return (
            <div className="App">
                <AddTodo addTodo={this.addTodo}/>
                <TodoList
                    todos={this.state.todos}
                    filterBy={this.state.filterBy}
                    toggleCompleted={this.toggleCompleted}
                />
                <Footer
                    setCompleterFilter={this.setCompleterFilter}
                    filterBy={this.state.filterBy}
                />
            </div>
        );
    }

    setCompleterFilter = (newFilterBy) => {
        this.setState({
            todos: this.state.todos,
            filterBy: newFilterBy
        });
    };
    addTodo = (todoTitle) => {
        const newTodos = this.state.todos;
        const newTodo = {
            title: todoTitle, completed: false
        };
        this.saveTodo(newTodo);
        newTodos.push(newTodo);
        this.setState({
            todos: newTodos, filterBy: this.state.filterBy
        });
    };
    toggleCompleted = (toggleTodo) => {
        const newState = {
            todos: this.state.todos.map((todo) => {
                if (todo !== toggleTodo) {
                    return todo;
                }
                const newTodo = Object.assign({}, todo, {
                    completed: !todo.completed
                });
                this.saveTodo(newTodo);
                return newTodo;
            }),
            filterBy: this.state.filterBy
        };
        this.setState(newState);
    };
    saveTodo(todo) {
        let method = 'POST';
        let url = TODOS_ENDPOINT;
        if (todo.id) {
            method = 'PUT';
            url += '/' + String(todo.id);
        }
        fetch(url, {
            method,
            headers: {'Content-Type': 'application/json',},
            body: JSON.stringify(todo)
        });
    }
}


export default App;
