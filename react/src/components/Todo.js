import React from 'react';

export default function Todo({todo, onClick}) {
    return <li onClick={onClick} className={todo.completed ? 'completed' : 'active'}>
        {todo.title}
    </li>
}
