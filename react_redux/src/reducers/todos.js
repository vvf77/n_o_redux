const todos = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return [
                ...state,
                {
                    id: action.id,
                    text: action.text,
                    completed: false
                }
            ];
        case 'TOGGLE_TODO':
            return state.map(todo =>
                (todo.id === action.id)
                    ? {...todo, completed: !todo.completed}
                    : todo
            );
        case 'LOAD_TODO':
            return [];
        case 'RECV_DATA':
            return action.data.map(({id, title, completed})=>({
                id, completed,
                text: title
            }));
        default:
            return state
    }
};

export default todos
