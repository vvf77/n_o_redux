import {combineReducers} from 'redux'
import todos from './todos'
import visibilityFilter from './visibilityFilter'
import isLoading from './isLoading'

export default combineReducers({
    todos,
    visibilityFilter,
    isLoading
})
