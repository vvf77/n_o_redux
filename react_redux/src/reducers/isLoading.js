const isLoading = (state = false, action) => {
    switch (action.type) {
        case 'REQ_DATA':
            return true;
        case 'RECV_DATA':
        case 'RECV_ERROR':
            return false;
        default:
            return state
    }
};

export default isLoading
