import React, {Component} from 'react'
import Footer from './Footer'
import AddTodo from '../containers/AddTodo'
import VisibleTodoList from '../containers/VisibleTodoList'
import {fetchTodos} from "../actions";

class App extends Component {
    componentWillMount(){
        this.props.dispatch(fetchTodos())
    }
    render() {
        if (this.props.isLoading) {
            return <div>
                <div className="loading">Loading...</div>
            </div>
        }
        return (
            <div>
                <AddTodo/>
                <VisibleTodoList/>
                <Footer/>
            </div>
        );
    }
}

export default App
