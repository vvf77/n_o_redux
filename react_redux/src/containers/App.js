import { connect } from 'react-redux'
import App from "../components/App";

const mapStateToProps = (state, ownProps) => ({
    isLoading: state.isLoading
});

export default connect(
    mapStateToProps
)(App)
