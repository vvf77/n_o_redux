let nextTodoId = 0;
export const addTodo = text => ({
    type: 'ADD_TODO',
    id: nextTodoId++,
    text
});

export const setVisibilityFilter = filter => ({
    type: 'SET_VISIBILITY_FILTER',
    filter
});

export const toggleTodo = id => ({
    type: 'TOGGLE_TODO',
    id
});

export const VisibilityFilters = {
    SHOW_ALL: 'SHOW_ALL',
    SHOW_COMPLETED: 'SHOW_COMPLETED',
    SHOW_ACTIVE: 'SHOW_ACTIVE'
};

const TODOS_ENDPOINT = 'https://jsonplaceholder.typicode.com/todos';

function requestData() {
    return {type: 'REQ_DATA'}
}

function receiveData(json) {
    return {
        type: 'RECV_DATA',
        data: json
    }
}

function receiveError(error) {
    return {
        type: 'RECV_ERROR',
        error
    }
}

export function fetchTodos() {
    return dispatch => {
        dispatch(requestData());
        return fetch(TODOS_ENDPOINT)
            .then(response =>  response.json())
            .then(data => {
                dispatch(receiveData(data));
            })
            .catch(function (error) {
                dispatch(receiveError(error));
            })
    }
}